# EU4 QoL Fix AHK Script

This is a simple AHK script to provide QoL improvements for Europa Universalis IV on Windows.

<h2>Hotkeys</h2>

1. Super+Z: Click infinite times at the cursor position to clear a build list in the macrobuilder.

2. Super+X: Automatically transfer occupation for selected province to certain country.

3. Super+S: Delete the building of a province in the first building slot

4. Super+A: Disband the selected land/naval unit

5. Super+C: Click once

6. Super+V: Grant a province to subject in the subject interaction tab, the subject interaction tab must be open and scrolled to bottom

7. Super+Q: Pause the script, press again to unpause

<h2>How to use</h2>

1. Install AutoHotKey from https://www.autohotkey.com/ or run `choco install autohotkey` if you have Chocolatey.

2. Run the script and test it in game, if the hotkey doesn't work, try run with admin privilege.

<h2>Editing the script</h2>

To use the function 2, 3, 4, 6, you will need to manually set up the cursor coordinates before using it. You can directly edit the script with notepad. The coordinates can be found using the "window spy" function of AHK, and it should be relative to the active window ("Window" type). You need to be change for a different subject each time.

<h2>Enjoy!</h2>
