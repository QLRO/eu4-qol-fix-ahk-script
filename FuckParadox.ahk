; EU4 QoL Fix Script
; Author: QLRO
; 17/2/2021

#z::
Loop, 2000
{
		Send {Click}
			Sleep, 200
}
return

#x::
MouseGetPos, xpos, ypos
Click, 324, 691	; Coordinate of "transfer occupation" button
Sleep, 100
Click, 863, 619 ; Coordinate of the scroll bar position
Sleep, 100
Click, 551, 410 ; Coordinate of the subject flag
Sleep, 100
Send {z}
MouseMove, xpos, ypos
return

#s::
MouseGetPos, xpos, ypos
Click
Sleep, 100
MouseMove, 539, 618 ; Coordinate of destory building button
Sleep, 100
Click
Sleep, 100
Send {c}
MouseMove, xpos, ypos
return


#a::
MouseGetPos, xpos, ypos
MouseMove, 396, 278 ; Coordinate of disband units button
Sleep, 100
Click
Sleep, 100
Send {c}
MouseMove, xpos, ypos
return


#c::
Click
return

#v::
MouseGetPos, xpos, ypos
Click, 735,513	; Coordinate of "Grant province" button
Sleep, 100
MouseMove, xpos, ypos
Sleep, 100
Click,
Sleep, 100
Send {Enter}
Sleep, 100
Send {c}
Sleep, 100
MouseMove, xpos, ypos
return

#q::
Pause
return
}
